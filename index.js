const Youtube = require("youtube-api"),
    fs = require("fs"),
    readJson = require("r-json"),
    express = require("express"),
    bodyParser = require("body-parser"),
    Logger = require("bug-killer"),
    // opn = require("opn"),
    prettyBytes = require("pretty-bytes"),
    request = require('request'),
    download = require('download'),
    mv = require('mv');


// I downloaded the file from OAuth2 -> Download JSON
const CREDENTIALS = readJson(`${__dirname}/credentials.json`);

// Init lien server
const port = 8080;
const RES = 360;

// Authenticate
// You can access the Youtube resources via OAuth2 only.
// https://developers.google.com/youtube/v3/guides/moving_to_oauth#service_accounts
let oauth = Youtube.authenticate({
    type: "oauth",
    client_id: CREDENTIALS.web.client_id,
    client_secret: CREDENTIALS.web.client_secret,
    redirect_url: CREDENTIALS.web.redirect_uris[0]
});

// opn(oauth.generateAuthUrl({
//     access_type: "offline",
//     scope: ["https://www.googleapis.com/auth/youtube.upload"]
// }));


const clientId = "cqgijagjvx1b0h8g17h48wa6wcq7bz";
const accept = "application/vnd.twitchtv.v5+json";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// Handle oauth2 callback
app.get('/oauth2callback', function (req, res) {
    Logger.log(req.query.code);
    Logger.log("Trying to get the token using the following code: " + req.query.code);
    oauth.getToken(req.query.code, (err, tokens) => {

        if (err) {
            res.status(400);
            res.send(err);
            return Logger.log(err);
        }

        Logger.log("Got the tokens.");

        oauth.setCredentials(tokens);

        res.send("The video is being uploaded. Check out the logs in the terminal.");
    });
});

app.get('/clips', (req, res) => {
    const options = {
        url: 'https://api.twitch.tv/kraken/clips/top?limit=10&game=Fortnite&trending=true&language=en&period=week',
        headers: {
            'Client-ID': clientId,
            'Accept': accept
        }
    };
    request(options, (error, response, body) => {
        if (!error && response.statusCode == 200) {
            var json = JSON.parse(body);
            json.clips.map(c => {
                let {
                    medium: url
                } = c.thumbnails;
                c.downloadUrl = url.includes("offset") ? url.replace("-preview-480x272.jpg", ".mp4") : url.replace("twitch.tv/", "twitch.tv/AT-cm%7C").replace("preview-480x272.jpg", `${RES}.mp4`)
                return {
                    ...c,
                    downloadUrl: url
                }
            })

            res.send(json);
        }
    });

});


app.post("/downloadclip", (req, res) => {
    const {
        url,
        title,
        desc
    } = req.body;
    const filename = url.split("/")[3];
    res.status(200);
    res.send("File downloaded in the background");

    download(url, 'downloading').then(() => {
        console.log('done!');
        mv(`downloading/${filename}`, `downloaded/${filename}`, function (err) {
            if (err) console.error(err);

            console.log("Moving files...");
            var req = Youtube.videos.insert({
                resource: {
                    snippet: {
                        title: title,
                        description: desc
                    },
                    status: {
                        privacyStatus: "private"
                    }
                },
                part: "snippet,status",
                media: {
                    body: fs.createReadStream(`downloaded/${filename}`)
                }
            }, (err, data) => {
                if (err) {
                    throw new Error("Error when inserting video");
                }

                console.log("Done.");
                Logger.log(data);
            });

            setInterval(function () {
                Logger.log(`${prettyBytes(req.req.connection._bytesDispatched)} bytes uploaded.`);
            }, 250);
        });
    });
})

app.listen(port, () => console.log(`Server running on port ${port}`));